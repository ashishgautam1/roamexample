package com.ashish.roamtest


import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.ashish.roamtest.data.RoamService
import com.ashish.roamtest.data.RoamPreference
import com.roam.sdk.Roam
import com.roam.sdk.RoamPublish
import com.roam.sdk.RoamTrackingMode
import com.roam.sdk.callback.RoamCallback
import com.roam.sdk.callback.RoamLocationCallback
import com.roam.sdk.models.RoamError
import com.roam.sdk.models.RoamUser
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {

    private lateinit var progressBar: ProgressBar
    private lateinit var userId: TextView
    private lateinit var long: TextView
    private lateinit var lat: TextView
    private lateinit var trackingStatus: TextView
    private lateinit var trackingInfo: TextView
    private lateinit var refreshButton: Button


    private val roamPreference = RoamPreference(context = this)
    private val hasUser = roamPreference.isSignedIn()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        requestPermission()
        Roam.startTracking(RoamTrackingMode.ACTIVE)
//        initialize()
        trackingInfo = findViewById(R.id.tv_info)
        trackingStatus = findViewById(R.id.tracking_status)
        refreshButton = findViewById(R.id.refresh_button)
        progressBar = findViewById(R.id.progressbar)

        publishAndSubscribe()
        Roam.offlineLocationTracking(true)
        Roam.disableBatteryOptimization()
        Roam.setDeviceToken(TOKEN)
        //Here 20 is interval  in second
        Roam.updateLocationWhenStationary(60)
        Roam.setTrackingInAppState(RoamTrackingMode.AppState.ALWAYS_ON)
        showTrackingDetails()

//        Roam.setForegroundNotification(true , "Roam", "Started" , )
        refreshButton.setOnClickListener {
            CommonUtils.log(TAG, "Refresh Clicked")
//            initialize()
            requestPermission()
            getLocationUpdate()
            trackingStatus()
        }

        CommonUtils.log(TAG, "FCM token is ${Roam.getDeviceToken()}")
    }


    private fun initialize() = lifecycleScope.launch {

        if (hasUser.first()) {
            CommonUtils.log(
                TAG,
                "We have user with userID ${roamPreference.getUser().first()}"
            )
            showTrackingDetails()
            hide()

        } else {
            CommonUtils.log(TAG, "User not found")
            createUser("User")
            refresh()
        }
    }

    private fun refresh() = lifecycleScope.launch {
        val hasUser = roamPreference.isSignedIn()
        if (hasUser.first()) {
            CommonUtils.log(
                TAG,
                "We have user with userID ${roamPreference.getUser().first()}"
            )
            showTrackingDetails()
            hide()

        } else {
            CommonUtils.log(TAG, "User not found")
            createUser("User")
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        CommonUtils.log(TAG, "Back Button Pressed ")
//        moveTaskToBack(false)
    }

    override fun onStop() {
        super.onStop()
        Roam.startTracking(RoamTrackingMode.ACTIVE)
    }


    private fun showTrackingDetails() {
        provideLogs()
        trackingStatus()
        getLocationUpdate()
    }

    private suspend fun createUser(description: String) {
        Roam.createUser(description, null, object : RoamCallback {
            override fun onSuccess(roamUser: RoamUser) {
                CommonUtils.log(
                    TAG,
                    "We have created New user with id ${roamPreference.getUser()}"
                )
                lifecycleScope.launch {
                    roamPreference.createUser(roamUser.userId)
                }
                show()
                showMsg("user created")

            }

            override fun onFailure(roamError: RoamError) {
                hide()
                CommonUtils.log(
                    MainActivity.TAG,
                    "There is an error while Creating RoamUser as ${roamError.message}"
                )
                showMsg("Fail to create user")

            }
        })

    }

    private fun showMsg(s: String) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show()

    }

    private fun trackingStatus() {
        if (Roam.isLocationTracking()) {
            CommonUtils.log(TAG, "Tracking Status ${Roam.isLocationTracking()}")
            startService(Intent(this, RoamService::class.java))
            trackingStatus.text = "Started"
            trackingStatus.setBackgroundColor(Color.GREEN)
            trackingStatus.setTextColor(Color.WHITE)
        } else {
            CommonUtils.log(TAG, "Tracking Status ${Roam.isLocationTracking()}")
            trackingStatus.text = "Stopped"
            stopService(Intent(this, RoamService::class.java))

            trackingStatus.setBackgroundColor(Color.RED)
            trackingStatus.setTextColor(Color.WHITE)


        }
    }


    private fun provideLogs() {
        Roam.toggleListener(true, true, object : RoamCallback {
            override fun onSuccess(roamUser: RoamUser) {
                // Access Roam user data below
                userId = findViewById(R.id.tv_roamUser)

                userId.text = roamUser.userId
                CommonUtils.log(TAG, "Data from ToggleListener userId ${roamUser.userId}")
                CommonUtils.log(TAG, "Data from ToggleListener description ${roamUser.description}")
                CommonUtils.log(
                    TAG,
                    "Data from ToggleListener locationEvents ${roamUser.locationEvents}"
                )
                CommonUtils.log(
                    TAG,
                    "Data from ToggleListener eventListenerStatus ${roamUser.eventListenerStatus}"
                )


            }

            override fun onFailure(roamError: RoamError) {
                CommonUtils.log(
                    TAG,
                    "Error from ToggleListener ${roamError.message} with code ${roamError.code}"
                )

                val info =
                    StringBuilder().append(roamError.message).append("with code ${roamError.code}")
                trackingInfo.text = info
            }
        })
    }

    private fun getLocationUpdate() {

        Roam.getCurrentLocation(RoamTrackingMode.DesiredAccuracy.MEDIUM, 100, object :
            RoamLocationCallback {
            override fun location(location: Location?) {
                CommonUtils.log(
                    TAG,
                    "RoamLocationCallback  long and latitude : ${location?.longitude}${location?.latitude}"
                )
                lat = findViewById(R.id.tv_latitude)
                long = findViewById(R.id.tv_longitude)
                // Access location data here
                lat.text = StringBuilder().append("lat-").append(location?.latitude.toString())
                long.text = StringBuilder().append("long-").append(location?.longitude.toString())
                trackingInfo.text =
                    StringBuilder().append("Accuracy-").append(location?.accuracy.toString())
            }

            override fun onFailure(roamError: RoamError) {
                // Access Error code and message here
                // roamError.code
                // roamError.message
                CommonUtils.log(TAG, " Error GetCurrentLocation Callback ${roamError.message} ")
            }
        })
    }


    private fun show() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hide() {
        progressBar.visibility = View.GONE
    }

    private fun publishAndSubscribe() {
        Roam.toggleListener(true, true, object : RoamCallback {
            override fun onSuccess(roamUser: RoamUser) {
                Roam.subscribe(Roam.Subscribe.LOCATION, roamUser.userId)
                val geoSparkPublish = RoamPublish.Builder()
                    .build()
                Roam.publishAndSave(geoSparkPublish)
            }

            override fun onFailure(roamError: RoamError) {}
        })
    }

    private fun requestPermission() {
        if (!Roam.checkLocationServices()) {
            Roam.requestLocationServices(this);
        } else if (!Roam.checkLocationPermission()) {
            Roam.requestLocationPermission(this);
            Toast.makeText(this@MainActivity, "Location Service granted", Toast.LENGTH_SHORT).show()

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && !Roam.checkBackgroundLocationPermission()) {
            Toast.makeText(this@MainActivity, "Location Permission granted", Toast.LENGTH_SHORT)
                .show()

            Roam.requestBackgroundLocationPermission(this);
        }
    }


    override fun onStart() {
        super.onStart()
        CommonUtils.log(TAG, "OnStart Called ")

    }


    override fun onResume() {
        super.onResume()
        CommonUtils.log(TAG, "OnResume - Tracking status ${Roam.isLocationTracking()} ")
//        Roam.startTracking(RoamTrackingMode.BALANCED)
    }

    override fun onDestroy() {
        super.onDestroy()
        CommonUtils.log(TAG, "OnDestroy - Tracking status ${Roam.isLocationTracking()} ")

//        Roam.stopTracking()
    }

    companion object {
        const val TAG = "MainActivity"
        const val TOKEN =
            "AAAAUBTYKnA:APA91bGAxoJSczTH6aUVP6OziPRvjpxlAe2xbVKbLG5W3OvtaUb_4PWyYACMAxxa_W6YGsX47YwLn2bByfzBrEhMamnDkUXjjCYwBKjTqHcSJSDTfOHlgh_uk0qBK6_qkOhvFXyqWzX0"
    }
}
