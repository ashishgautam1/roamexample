package com.ashish.roamtest

import android.content.Context
import android.util.Log
import com.bugfender.sdk.Bugfender

object CommonUtils {
    fun log(tag: String, message: String) {
//        Bugfender.d(tag, message)
        Log.d(tag, message)
    }
}