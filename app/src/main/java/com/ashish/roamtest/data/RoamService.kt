package com.ashish.roamtest.data

import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import com.ashish.roamtest.LocationReceiver


class RoamService : Service() {
    private var mLocationReceiver: LocationReceiver? = null
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        register()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        unRegister()
    }

    private fun register() {
        mLocationReceiver = LocationReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("com.roam.android.RECEIVED")
        registerReceiver(mLocationReceiver, intentFilter)
    }

    private fun unRegister() {
        if (mLocationReceiver != null) {
            unregisterReceiver(mLocationReceiver)
        }
    }
}