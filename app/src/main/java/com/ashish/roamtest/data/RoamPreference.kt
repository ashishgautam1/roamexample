package com.ashish.roamtest.data

import android.content.Context
import androidx.datastore.DataStore
import androidx.datastore.createDataStore
import androidx.datastore.preferences.Preferences
import androidx.datastore.preferences.createDataStore
import androidx.datastore.preferences.edit
import androidx.datastore.preferences.preferencesKey
import com.roam.sdk.Roam
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


class RoamPreference(context: Context) {

    private val dataStore: DataStore<Preferences> = context.createDataStore(name = "user")
    suspend fun createUser(userId : String){
      dataStore.edit {
          it[ROAM_USER] = userId
      }
    }

   fun getUser() : Flow<String> = dataStore.data.map {
        it[ROAM_USER] ?: "No User Yet"
    }

    fun isSignedIn(): Flow<Boolean> =
        dataStore.data.map {
            it.contains(ROAM_USER)
        }



    companion object {
       private val ROAM_USER = preferencesKey<String>("roam_user")
    }
}