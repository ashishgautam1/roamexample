package com.ashish.roamtest

import android.content.Context
import android.widget.Toast
import com.roam.sdk.models.RoamError
import com.roam.sdk.models.RoamLocation
import com.roam.sdk.models.events.RoamEvent
import com.roam.sdk.service.RoamReceiver


class LocationReceiver : RoamReceiver() {

    override fun onLocationUpdated(context: Context?, list: List<RoamLocation>) {
        super.onLocationUpdated(context, list)
        CommonUtils.log(
            TAG,
            "Lat " + list[0].location.latitude + " Lng " + list[0].location.longitude
        )
          Toast.makeText(context, "Location: "+"Lat: "+list[0].location.latitude +" Lng: "+list[0].location.longitude, Toast.LENGTH_LONG).show();
    }

    override fun onEventReceived(context: Context?, roamEvent: RoamEvent) {
        super.onEventReceived(context, roamEvent)
        CommonUtils.log(
            TAG,
            "Geofence Id:  " + roamEvent.geofence_id + " Event Type:  " + roamEvent.event_type
        )
    }

    override fun onError(context: Context?, roamError: RoamError) {
        CommonUtils.log(TAG, roamError.message)
    }
    companion object{
        val TAG = "LocationReceiver"
    }
}