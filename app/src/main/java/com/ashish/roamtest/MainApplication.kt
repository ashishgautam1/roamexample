package com.ashish.roamtest

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import com.bugfender.sdk.Bugfender
import com.roam.sdk.Roam

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Roam.initialize(this , "8655908aca888406f685109c97538ceb904a786f492550d30c56b1d98a92a529")
        Bugfender.init(this, "bl6pVjuJCeux5Dex4uENtYocgjdvVRL2", BuildConfig.DEBUG)
        Bugfender.enableCrashReporting()
        Bugfender.enableUIEventLogging(this)
        Bugfender.enableLogcatLogging()
    }

//    @OnLifecycleEvent(Lifecycle.Event.ON_START)
//    fun appInForeground() {
//        CommonUtils.log("Main", "App is in forground and tracking status is ${Roam.isLocationTracking()}")
//    }
//
//    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
//    fun appInBackground() {
//        CommonUtils.log("Main", "App is in background   and tracking status is ${Roam.isLocationTracking()}")
//    }
}